﻿using Rocket.API;
using Rocket.Core.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RocketMod_ItemRestriction
{
    public class IR_Config : IRocketPluginConfiguration
    {
        [XmlArrayItem(ElementName = "Item")]
        public List<Item> Items;

        public bool ignoreAdmin;

        public void LoadDefaults()
        {
            this.ignoreAdmin = true;
            this.Items = new List<Item>()
            {
                new Item(519),
                new Item(1050)
            };
        }
    }
}
