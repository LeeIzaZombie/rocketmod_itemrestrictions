﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketMod_ItemRestriction
{
    public class Item
    {
        public ushort ID;
        private Item() { }

        public Item(ushort id)
        {
            ID = id;
        }
    }
}
